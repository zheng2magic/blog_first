const { User } = require('../../model/user')
module.exports = async(req, res) => {

    //标识，标识当前访问的是用户管理页面
    req.app.locals.currentLink = 'user'

    //接受客户端传来的当前页参数
    let page = req.query.page || 1

    //自定每页条数
    let pagesize = 10

    //查询总数
    let count = await User.countDocuments({})

    //计算总页数（总数据/每页条数）
    let total = Math.ceil(count / pagesize)

    //页码对应开始位置
    let start = (page - 1) * pagesize

    //查询用户信息
    let users = await User.find({}).limit(pagesize).skip(start)

    res.render('admin/user', {
        users: users,
        page: page,
        total: total
    })
}