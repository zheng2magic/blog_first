const formidable = require('formidable');
const path = require('path');
const { Article } = require('../../model/article')

module.exports = (req, res) => {
    //创建表单解析对象
    const form = new formidable.IncomingForm()
        //配置上传地址 /public/upload
    form.uploadDir = path.join(__dirname, '../', '../', 'public', 'uploads')
        //保留后缀，默认不保留
    form.keepExtensions = true
        //解析
    form.parse(req, async(err, fields, files) => {
            // res.send(files)
            await Article.create({
                title: fields.title,
                author: fields.author,
                publishDate: fields.publishDate,
                cover: files.cover.path.split('public')[1],
                content: fields.content

            })
            res.redirect('/admin/article')
        })
        // res.send(form)
}