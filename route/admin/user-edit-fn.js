const { User, validateUser } = require('../../model/user');
const bcrypt = require('bcrypt');

module.exports = async(req, res, next) => {

    try {
        await validateUser(req.body)
    } catch (err) {
        //验证没通过
        //重定向
        // return res.redirect(`/admin/user-edit?message=${error.message}`)
        return next(JSON.stringify({ path: '/admin/user-edit', message: err.message }))
    }

    //根据邮箱查询用户是否存在
    let user = await User.findOne({ email: req.body.email })

    //如果用户存在，说明被占用
    if (user) {
        // return res.redirect(`/admin/user-edit?message=邮箱地址被占用`)
        return next(JSON.stringify({ path: '/admin/user-edit', message: '邮箱地址被占用' }))
    }

    const salt = await bcrypt.genSalt(10)
    const password = await bcrypt.hash(req.body.password, salt)

    //替换密码
    req.body.password = password

    //用户信息添加到数据库
    await User.create(req.body)

    //重定向
    res.redirect('/admin/user')

    res.send(password)

}