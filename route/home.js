// 展示页面路由


const express = require('express')

// 创建路由
const home = express.Router()

//登录
home.get('/login', require('./home/loginPage'))

//实现登录
home.post('/login', require('./home/login'))

home.get('/', require('./home/index'))

home.get('/article', require('./home/article'))

home.post('/comment', require('./home/comment'))

// 将路由对象作为模块成员导出
module.exports = home