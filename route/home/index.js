const { Article } = require('../../model/article')
const pagination = require('mongoose-sex-page')

module.exports = async(req, res) => {

    const page = req.query.page
        // res.send('欢迎来到首页')
    let result = await pagination(Article).page(page).size(4).display(5).find().populate('author').exec()


    res.render('home/default.art', {
        result: JSON.parse(JSON.stringify(result))
    })

}