const { Article } = require('../../model/article')
const { Comment } = require('../../model/comment')
module.exports = async(req, res) => {

    const id = req.query.id

    //查文章
    let article = await Article.findOne({ _id: id }).populate('author')

    //查评论
    let comments = await Comment.find({ aid: id }).populate('uid')



    res.render('home/article.art', {
        article: JSON.parse(JSON.stringify(article)),
        comments: JSON.parse(JSON.stringify(comments))
    })
}