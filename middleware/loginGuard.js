const guard = (req, res, next) => {
    //判断访问的是否是登陆页面
    //判断登录状态
    //登录，放行
    //没登录，重定向登录状态
    if (req.url != '/login' && !req.session.username) {
        res.redirect('/admin/login')
    } else {
        if (req.session.role == 'normal') {
            return res.redirect('/home/')
        }
        //已经登录，请求放行
        next()
    }
}

module.exports = guard