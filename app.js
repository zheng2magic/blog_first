//应用express
const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const session = require('express-session')
const dateFormat = require('dateformat');
const template = require('art-template');

//创建服务器
const app = express()

//应用数据库模块
require('./model/connect')

//处理post请求
app.use(bodyParser.urlencoded({ extended: false }))
    //配置session
app.use(session({
    secret: 'secret key',
    resave: true,
    saveUninitialized: false,
    cookie: {
        maxAge: 24 * 60 * 60 * 1000
    }
}))

//告诉express模板的位置
app.set('views', path.join(__dirname, 'views'))

//告诉express模板的默认后缀
app.set('view engine', 'art')

//当渲染后缀art时，使用的引擎是什么
app.engine('art', require('express-art-template'))

//向模板内部导入变量
template.defaults.imports.dateFormat = dateFormat

//开放静态资源文件
app.use(express.static(path.join(__dirname, 'public')))

// 引入路由模块
const home = require('./route/home')
const admin = require('./route/admin')
const { nextTick } = require('process')

//拦截请求，判断登陆状态
app.use('/admin', require('./middleware/loginGuard'))

// 为路由匹配路径
app.use('/home', home)
app.use('/admin', admin)

//错误处理
app.use((err, req, res, next) => {
    //将字符串转换成对象 JSON.parse()
    const result = JSON.parse(err)
    let params = []
    for (let attr in result) {
        if (attr != path) {
            params.push(attr + '=' + result[attr])
        }
    }
    res.redirect(`${result.path}?${params.join('&')}`)
})

// 监听端口
app.listen(80)
console.log('启动成功');